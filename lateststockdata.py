import cPickle
import numpy as np
import pandas as pd
import datetime
from sklearn import preprocessing
#from datetime import datetime
from sklearn.ensemble import RandomForestClassifier
from sklearn import neighbors
from sklearn.ensemble import AdaBoostClassifier
from sklearn.ensemble import GradientBoostingClassifier
from sklearn.svm import SVC
import operator
import pandas_datareader.data as web
from sklearn.qda import QDA
import re
from dateutil import parser
import os
#from backtest import Strategy, Portfolio
from abc import ABCMeta, abstractmethod
import json
import poloniex
import time
import calendar

def fcl(df, dtObj):
    return df.iloc[np.argmin(np.abs(df.index.to_pydatetime() - dtObj))]

def roundTime(dt=None, dateDelta=datetime.timedelta(minutes=1)):
    """Round a datetime object to a multiple of a timedelta
    dt : datetime.datetime object, default now.
    dateDelta : timedelta object, we round to a multiple of this, default 1 minute.
    Author: Thierry Husson 2012 - Use it as you want but don't blame me.
            Stijn Nevens 2014 - Changed to use only datetime objects as variables
    """
    roundTo = dateDelta.total_seconds()

    if dt == None : dt = datetime.datetime.now()
    seconds = (dt - dt.min).seconds
    # // is a floor division, not a comment on following line:
    rounding = (seconds+roundTo/2) // roundTo * roundTo
    return dt + datetime.timedelta(0,rounding-seconds,-dt.microsecond)

def loadDatasets(path_datasets, fout):
    return cPickle.load(open( os.path.join(path_datasets,fout), "rb" ) )

def getCryptoCoin(symbol, start, end, period):
    polo = poloniex.Poloniex()
    polo.timeout = 20
    start = int(time.mktime(start.timetuple()))
    end = int(calendar.timegm(end.timetuple()))
    #period =  valid values are 300, 900, 1800, 7200, 14400, and 86400), 
    ticker = pd.DataFrame(polo.api('returnChartData',{'currencyPair':symbol,'period':period,'start':start, 'end':9999999999}))
    
    ticker = ticker.drop('quoteVolume', 1)
    ticker = ticker.drop('weightedAverage', 1)
    ticker = ticker.rename(columns={'date':'Date', 'open':'Open', 'high':'High', 'low':'Low', 'close':'Close', 'volume':'Volume'})
    ticker['Date'] = pd.to_datetime(ticker['Date'],unit='s')
    ticker = ticker.set_index('Date')
    ticker['AdjClose'] = ticker['Close']
    ticker = ticker[['Open', 'High', 'Low', 'Close', 'Volume', 'AdjClose']]
    ticker.columns = ticker.columns + '_' + symbol
    ticker['Return_%s' %symbol] = ticker['AdjClose_%s' %symbol].pct_change()

    return ticker

def getStockDataFromWeb(fout, start_string, end_string, period):
    """
    Collects predictors data from Yahoo Finance and Quandl.
    Returns a list of dataframes.
    """
    start = parser.parse(start_string)
    end = parser.parse(end_string)
    #period =  valid values are 300, 900, 1800, 7200, 14400, and 86400 

    #eth = getCryptoCoin('USDT_ETH', start, end, period)
    btc = getCryptoCoin('USDT_BTC', start, end, period)

    polo = poloniex.Poloniex()
    polo.timeout = 5
    start = int(time.mktime(start.timetuple()))
    end = int(calendar.timegm(end.timetuple()))
    #period =  valid values are 300, 900, 1800, 7200, 14400, and 86400 
    out = pd.DataFrame(polo.api('returnChartData',{'currencyPair':fout,'period':period,'start':start, 'end':end}))
    
    out = out.drop('quoteVolume', 1)
    out = out.drop('weightedAverage', 1)
    out = out.rename(columns={'date':'Date', 'open':'Open', 'high':'High', 'low':'Low', 'close':'Close', 'volume':'Volume'})
    out['Date'] = pd.to_datetime(out['Date'],unit='s')
    out = out.set_index('Date')
    out['AdjClose'] = out['Close']
    out = out[['Open', 'High', 'Low', 'Close', 'Volume', 'AdjClose']]
    out.columns = out.columns + '_Out'
    out['Return_Out'] = out['AdjClose_Out'].pct_change()
    
    return [out, btc]
    #return [out, eth, btc]
	

def addFeatures(dataframe, adjclose, returns, n):
    """
    operates on two columns of dataframe:
    - n >= 2
    - given Return_* computes the return of day i respect to day i-n. 
    - given AdjClose_* computes its moving average on n days

    """
    
    return_n = adjclose[9:] + "Time" + str(n)
    dataframe[return_n] = dataframe[adjclose].pct_change(n)
    
    roll_n = returns[7:] + "RolMean" + str(n)
    dataframe[roll_n] = pd.rolling_mean(dataframe[returns], n)
	
def applyRollMeanDelayedReturns(datasets, delta):
    """
    applies rolling mean and delayed returns to each dataframe in the list
    """
    for dataset in datasets:
        columns = dataset.columns    
        adjclose = columns[-2]
        returns = columns[-1]
        for n in delta:
            addFeatures(dataset, adjclose, returns, n)
    #print datasets
    return datasets

def mergeDataframes(datasets, index, cut):
    """
    merges datasets in the list 
    applying a time cut at the beginning of the series
    This is the step in which we get rid of all the Open, High, Low, Close, Volume, AdjClose 
    columns in each dataset and keep only 
    the Daily Returns, delta-Multiple Day Returns and delta-Returns Moving Average previously created.
    """
    #subset = []tion
    print "merging dataframes"
    subset = [dataset.iloc[:, index:] for dataset in datasets[1:]]
    print 'made subset'
    #print subset[0]
    first = subset[0].join(subset[1:], how = 'outer')
    print 'made first'
    finance = datasets[0].iloc[:, index:].join(first, how = 'left')
    #finance = finance[finance.index > cut]
    print 'returning finance'
    return finance
	
def applyTimeLag(dataset, lags, delta):
    """
    apply time lag to return columns selected according  to delta.
    Days to lag are contained in the lads list passed as argument.
    Returns a NaN free dataset obtained cutting the lagged dataset
    at head and tail
    """
    dataset.Return_Out = dataset.Return_Out.shift(-1)
    maxLag = max(lags)
    #print dataset

    columns = dataset.columns[::(2*max(delta)-1)]
    for column in columns:
        for lag in lags:
            newcolumn = column + str(lag)
            dataset[newcolumn] = dataset[column].shift(lag)

    return dataset.iloc[maxLag:-1,:]

start_date = "2016-6-08"
cut = parser.parse(start_date)
end_date = str(datetime.datetime.utcnow())
#end_date = str(datetime.datetime(2016,6,16))

#end_date = datetime.date.today() - datetime.timedelta(days=1)
period = 14400
stock = 'USDT_BTC'
bestlags = 20
bestdelta = 20
best_model = 'optimised_best_'+stock+'-.pickle'

datasets = getStockDataFromWeb(stock, start_date, end_date, period)
cPickle.dump(datasets, open( "latestdatasets"+stock+".pickle", "wb" ) )

datasets = loadDatasets(os.path.dirname(os.path.realpath("__file__")), "latestdatasets"+stock+".pickle")

#print datasets

#lags = range(2, bestlags +1) 
#delta = range(2, bestdelta +1) 
lags = range(1, bestlags) 
delta = range(1, bestdelta) 
datasets = applyRollMeanDelayedReturns(datasets, delta)
print "Applied roll mean delayed returns"
finance = mergeDataframes(datasets, 6, cut)
print 'Size of data frame: ', finance.shape
print 'Number of NaN after merging: ', finance.isnull().sum().sum()
finance = finance.interpolate(method='linear')
print 'Number of NaN after linear interpolation: ', finance.isnull().sum().sum() 
finance = finance.fillna(finance.mean())
print 'Number of NaN after mean interpolation: ', finance.isnull().sum().sum() 
finance = applyTimeLag(finance, lags, delta)
print 'Number of NaN after temporal shifting: ', finance.isnull().sum().sum() 
print 'Size of data frame after feature creation: ', finance.shape

le = preprocessing.LabelEncoder()
finance['UpDown'] = finance['Return_Out']
#finance = finance[:-1]
finance.UpDown[finance.UpDown > 0] = 'Up'
finance.UpDown[finance.UpDown <= 0] = 'Down'
#print finance['UpDown']
finance.UpDown = le.fit(finance.UpDown).transform(finance.UpDown)
#print finance

# get rid of UpDown column on the end of finance
features = finance.columns[1:-1]
X_test = finance[features]
Y_test = finance.UpDown

#open the best model made by findbestmodel.py
with open(best_model, 'rb') as fin:
    model = cPickle.load(fin)

prediction = []
probUp = []
probDown = []
#print X_test['Return_USDT_BTC']
for down, up in model.predict_proba(X_test):
    #print up, down
    direction = up - down
    probUp.extend([up])
    probDown.extend([down])
    #print direction
    if abs(direction) >= 0.1:
        if direction > 0:
            prediction.extend([1])
        else:
            prediction.extend([0])
    else:
        prediction.extend([0])

#print finance
#print prediction

finance['Up'] = probUp
finance['Dn'] = probDown
#finance.Return_Out = finance.Return_Out.shift(1)
#finance.UpDown = finance.UpDown.shift(1)
finance['Pred'] = prediction
finance.UpDown[finance.UpDown == 1] = 'Up'
finance.UpDown[finance.UpDown == 0] = 'Down'
print finance[['Return_Out','UpDown','Pred', 'Up', 'Dn']]



# get current UTC time
tm = datetime.datetime.utcnow()
# round down to nearest 4 hour block
# minus 4 hours to get previous block, because this is where our prediction about the current period exists
tm = tm - datetime.timedelta(hours=tm.hour % 4,minutes=tm.minute,seconds=tm.second,microseconds=tm.microsecond) - datetime.timedelta(hours=4)
# to overcome a bug, call a function which finds the closest time in our index to the one we work out above
print fcl(finance, tm)
predictionForTimePeriod = fcl(finance, tm)['Pred']
print predictionForTimePeriod