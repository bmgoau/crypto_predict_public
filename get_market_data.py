import cPickle
import numpy as np
import pandas as pd
import datetime
from sklearn import preprocessing
#from datetime import datetime
from sklearn.ensemble import RandomForestClassifier
from sklearn import neighbors
from sklearn.ensemble import AdaBoostClassifier
from sklearn.ensemble import GradientBoostingClassifier
from sklearn.svm import SVC
import operator
import pandas_datareader.data as web
from sklearn.qda import QDA
import re
from dateutil import parser
import os
#from backtest import Strategy, Portfolio
from abc import ABCMeta, abstractmethod
import poloniex
import time

def getCryptoCoin(symbol, start, end, period):
    polo = poloniex.Poloniex()
    polo.timeout = 20
    start = int(time.mktime(start.timetuple()))
    end = int(time.mktime(end.timetuple()))
    #period =  valid values are 300, 900, 1800, 7200, 14400, and 86400), 
    ticker = pd.DataFrame(polo.api('returnChartData',{'currencyPair':symbol,'period':period,'start':start, 'end':end}))
    
    ticker = ticker.drop('quoteVolume', 1)
    ticker = ticker.drop('weightedAverage', 1)
    ticker = ticker.rename(columns={'date':'Date', 'open':'Open', 'high':'High', 'low':'Low', 'close':'Close', 'volume':'Volume'})
    ticker['Date'] = pd.to_datetime(ticker['Date'],unit='s')
    ticker = ticker.set_index('Date')
    ticker['AdjClose'] = ticker['Close']
    ticker = ticker[['Open', 'High', 'Low', 'Close', 'Volume', 'AdjClose']]
    ticker.columns = ticker.columns + '_' + symbol
    ticker['Return_%s' %symbol] = ticker['AdjClose_%s' %symbol].pct_change()

    return ticker

def getStockDataFromWeb(fout, start_string, end_string, period):
    """
    Collects predictors data from Yahoo Finance and Quandl.
    Returns a list of dataframes.
    """
    start = parser.parse(start_string)
    end = parser.parse(end_string)
    #period =  valid values are 300, 900, 1800, 7200, 14400, and 86400 

    #eth = getCryptoCoin('USDT_ETH', start, end, period)
    btc = getCryptoCoin('USDT_BTC', start, end, period)

    polo = poloniex.Poloniex()
    polo.timeout = 5
    start = int(time.mktime(start.timetuple()))
    end = int(time.mktime(end.timetuple()))
    #period =  valid values are 300, 900, 1800, 7200, 14400, and 86400 
    out = pd.DataFrame(polo.api('returnChartData',{'currencyPair':fout,'period':period,'start':start, 'end':end}))
    
    out = out.drop('quoteVolume', 1)
    out = out.drop('weightedAverage', 1)
    out = out.rename(columns={'date':'Date', 'open':'Open', 'high':'High', 'low':'Low', 'close':'Close', 'volume':'Volume'})
    out['Date'] = pd.to_datetime(out['Date'],unit='s')
    out = out.set_index('Date')
    out['AdjClose'] = out['Close']
    out = out[['Open', 'High', 'Low', 'Close', 'Volume', 'AdjClose']]
    out.columns = out.columns + '_Out'
    out['Return_Out'] = out['AdjClose_Out'].pct_change()
    
    return [out, btc]
    #return [out, eth, btc]

start_date = "2015-2-20"
end_date = "2017-5-18"
period = 14400
stock = 'USDT_BTC'

datasets = getStockDataFromWeb(stock, start_date, end_date, period)

print datasets

cPickle.dump(datasets, open( "datasets"+stock+".pickle", "wb" ) )
