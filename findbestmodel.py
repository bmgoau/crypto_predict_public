import cPickle
import numpy as np
import pandas as pd
import datetime
from sklearn import preprocessing
#from datetime import datetime
from sklearn.ensemble import RandomForestClassifier
from sklearn import neighbors
from sklearn.ensemble import AdaBoostClassifier
from sklearn.ensemble import GradientBoostingClassifier
from sklearn.svm import SVC
from sklearn import linear_model, datasets, metrics
from sklearn.cross_validation import train_test_split
from sklearn.neural_network import BernoulliRBM
from sklearn.pipeline import Pipeline
import operator
import pandas_datareader.data as web
from sklearn.qda import QDA
import re
from dateutil import parser
import os
from backtestlib import Strategy, Portfolio
from abc import ABCMeta, abstractmethod
import poloniex
import time

def loadDatasets(path_datasets, fout):
    return cPickle.load(open( os.path.join(path_datasets,fout), "rb" ) )

def getCryptoOut(symbol, start, end, period):
    """
    Downloads Stock from Yahoo Finance.
    Computes daily Returns based on Adj Close.
    Returns pandas dataframe.
    """
    polo = poloniex.Poloniex()
    polo.timeout = 5
    start = int(time.mktime(start.timetuple()))
    end = int(time.mktime(end.timetuple()))
    #period =  valid values are 300, 900, 1800, 7200, 14400, and 86400 
    out = pd.DataFrame(polo.api('returnChartData',{'currencyPair':symbol,'period':period,'start':start, 'end':end}))
    
    out = out.drop('quoteVolume', 1)
    out = out.drop('weightedAverage', 1)
    out = out.rename(columns={'date':'Date', 'open':'Open', 'high':'High', 'low':'Low', 'close':'Close', 'volume':'Volume'})
    out['Date'] = pd.to_datetime(out['Date'],unit='s')
    out = out.set_index('Date')
    out['AdjClose'] = out['Close']
    out = out[['Open', 'High', 'Low', 'Close', 'Volume', 'AdjClose']]
    out.columns = out.columns + '_Out'
    out['Return_Out'] = out['AdjClose_Out'].pct_change()
    
    return out
	

def addFeatures(dataframe, adjclose, returns, n):
    """
    operates on two columns of dataframe:
    - n >= 2
    - given Return_* computes the return of day i respect to day i-n. 
    - given AdjClose_* computes its moving average on n days

    """
    
    return_n = adjclose[9:] + "Time" + str(n)
    dataframe[return_n] = dataframe[adjclose].pct_change(n)
    
    roll_n = returns[7:] + "RolMean" + str(n)
    dataframe[roll_n] = pd.rolling_mean(dataframe[returns], n)
	
def applyRollMeanDelayedReturns(datasets, delta):
    """
    applies rolling mean and delayed returns to each dataframe in the list
    """
    for dataset in datasets:
        columns = dataset.columns    
        adjclose = columns[-2]
        returns = columns[-1]
        for n in delta:
            addFeatures(dataset, adjclose, returns, n)
    
    return datasets

def mergeDataframes(datasets, index, cut, end_period):
    """
    merges datasets in the list 
    applying a time cut at the beginning of the series
    This is the step in which we get rid of all the Open, High, Low, Close, Volume, AdjClose 
    columns in each dataset and keep only 
    the Daily Returns, delta-Multiple Day Returns and delta-Returns Moving Average previously created.
    """
    #subset = []tion
    subset = [dataset.iloc[:, index:] for dataset in datasets[1:]]
    
    first = subset[0].join(subset[1:], how = 'outer')
    finance = datasets[0].iloc[:, index:].join(first, how = 'left') 
    finance = finance[finance.index > cut]
    finance = finance[finance.index <= end_period]
    return finance
	
def applyTimeLag(dataset, lags, delta):
    """
    apply time lag to return columns selected according  to delta.
    Days to lag are contained in the lads list passed as argument.
    Returns a NaN free dataset obtained cutting the lagged dataset
    at head and tail
    """
    
    dataset.Return_Out = dataset.Return_Out.shift(-1)
    maxLag = max(lags)

    columns = dataset.columns[::(2*max(delta)-1)]
    for column in columns:
        for lag in lags:
            newcolumn = column + str(lag)
            dataset[newcolumn] = dataset[column].shift(lag)

    return dataset.iloc[maxLag:-1,:]
	
def prepareDataForClassification(dataset, start_test):
    """
    generates categorical output column, attach to dataframe 
    label the categories and split into train and test
    """
    le = preprocessing.LabelEncoder()
    
    dataset['UpDown'] = dataset['Return_Out']
    dataset.UpDown[dataset.UpDown >= 0] = 'Up'
    dataset.UpDown[dataset.UpDown < 0] = 'Down'
    dataset.UpDown = le.fit(dataset.UpDown).transform(dataset.UpDown)
    
    features = dataset.columns[1:-1]
    X = dataset[features]
    y = dataset.UpDown
    
    X_train = X[X.index < start_test]
    y_train = y[y.index < start_test]              
    
    X_test = X[X.index >= start_test]    
    y_test = y[y.index >= start_test]

    return X_train, y_train, X_test, y_test

		
def performRFClass(X_train, y_train, X_test, y_test, parameters, fout, savemodel):
    """
    Random Forest Binary Classification
    """
    clf = RandomForestClassifier(n_estimators=200, class_weight="auto", n_jobs=-1, verbose=0, warm_start=True)
    clf.fit(X_train, y_train)
    
    if savemodel == True:
        fname_out = '{}-.pickle'.format(fout)
        with open(fname_out, 'wb') as f:
            cPickle.dump(clf, f, -1)    
    
    accuracy = clf.score(X_test, y_test)
    
    return accuracy
	
def performClassification(X_train, y_train, X_test, y_test, method, parameters, stock, savemodel):
    """
    performs classification on daily returns using several algorithms (method).
    method --> string algorithm
    parameters --> list of parameters passed to the classifier (if any)
    stock --> string with name of stock to be predicted
    savemodel --> boolean. If TRUE saves the model to pickle file
    """
   
    if method == 'RF':   
        return performRFClass(X_train, y_train, X_test, y_test, parameters, stock, savemodel)
        
    elif method == 'KNN':
        return performKNNClass(X_train, y_train, X_test, y_test, parameters, stock, savemodel)
    
    elif method == 'SVM':   
        return performSVMClass(X_train, y_train, X_test, y_test, parameters, stock, savemodel)
    
    elif method == 'ADA':
        return performAdaBoostClass(X_train, y_train, X_test, y_test, parameters, stock, savemodel)
    
    elif method == 'GTB': 
        return performGTBClass(X_train, y_train, X_test, y_test, parameters, stock, savemodel)

    elif method == 'QDA': 
        return performQDAClass(X_train, y_train, X_test, y_test, parameters, stock, savemodel)

    elif method == 'RBM': 
        return performBernoulliRBM(X_train, y_train, X_test, y_test, parameters, stock, savemodel)


def performClassifierComparison(maxdeltas, maxlags, fout, cut, end_period, start_test, path_datasets, savemodel, method, parameters, stock):
    """
    Performs Feature selection for a specific algorithm
    """
    bestaccuracy = 0 
    bestlag = 0
    bestdelta = 0
    for maxlag in range(3, maxlags + 2):
        lags = range(2, maxlag)
        print '============================================================='
        print 'Maximum time lag applied', max(lags)
        print ''
        for maxdelta in range(3, maxdeltas + 2):
            datasets = loadDatasets(path_datasets, fout)
            delta = range(2, maxdelta) 
            print 'Delta days accounted: ', max(delta)
            datasets = applyRollMeanDelayedReturns(datasets, delta)
            finance = mergeDataframes(datasets, 6, cut, end_period)
            #print 'Size of data frame: ', finance.shape
            #print 'Number of NaN after merging: ', finance.isnull().sum().sum()
            finance = finance.interpolate(method='linear')
            #print 'Number of NaN after time interpolation: ', finance.isnull().sum().sum()
            finance = finance.fillna(finance.mean())
            #print 'Number of NaN after mean interpolation: ', finance.isnull().sum().sum()   
            finance = applyTimeLag(finance, lags, delta)
            #print 'Number of NaN after temporal shifting: ', finance.isnull().sum().sum()
            #print 'Size of data frame after feature creation: ', finance.shape
            X_train, y_train, X_test, y_test  = prepareDataForClassification(finance, start_test)
            
            accuracy = performClassification(X_train, y_train, X_test, y_test, method, parameters, stock, savemodel)
            if accuracy > bestaccuracy:
                bestaccuracy = accuracy
                bestlag = max(lags)
                bestdelta = max(delta)
            print accuracy
    print 'Best Accuracy: ', bestaccuracy
    print 'Best Lag: ', bestlag
    print 'Best Delta: ', bestdelta

def performFeatureSelection(maxdeltas, maxlags, fout, cut, end_period, start_test, path_datasets, savemodel, method, folds, parameters):
    """
    Performs Feature selection for a specific algorithm
    """
    
    for maxlag in range(3, maxlags + 2):
        lags = range(2, maxlag) 
        print ''
        print '============================================================='
        print 'Maximum time lag applied', max(lags)
        print ''
        for maxdelta in range(3, maxdeltas + 2):
            datasets = loadDatasets(path_datasets, fout)
            delta = range(2, maxdelta) 
            print 'Delta days accounted: ', max(delta)
            datasets = applyRollMeanDelayedReturns(datasets, delta)
            finance = mergeDataframes(datasets, 6, cut, end_period)
            print 'Size of data frame: ', finance.shape
            print 'Number of NaN after merging: ', count_missing(finance)
            finance = finance.interpolate(method='linear')
            print 'Number of NaN after time interpolation: ', count_missing(finance)
            finance = finance.fillna(finance.mean())
            print 'Number of NaN after mean interpolation: ', count_missing(finance)    
            finance = applyTimeLag(finance, lags, delta)
            print 'Number of NaN after temporal shifting: ', count_missing(finance)
            print 'Size of data frame after feature creation: ', finance.shape
            X_train, y_train, X_test, y_test  = prepareDataForClassification(finance, start_test)
            
            print performCV(X_train, y_train, folds, method, parameters, fout, savemodel)
            print ''

def getPredictionFromBestModel(bestdelta, bestlags, fout, cut, end_period, start_test, path_datasets, best_model):
    """
    returns array of prediction and score from best model.
    """
    lags = range(1, bestlags) 
    datasets = loadDatasets(path_datasets, fout)
    delta = range(1, bestdelta) 
    datasets = applyRollMeanDelayedReturns(datasets, delta)
    finance = mergeDataframes(datasets, 6, cut, end_period)
    finance = finance.interpolate(method='linear')
    finance = finance.fillna(finance.mean())
    finance = applyTimeLag(finance, lags, delta)
    X_train, y_train, X_test, y_test  = prepareDataForClassification(finance, start_test)    
    with open(best_model, 'rb') as fin:
        model = cPickle.load(fin)

    prediction = []

    for down, up in model.predict_proba(X_test):
        #print down, up
        direction = up - down
        if abs(direction) >= 0.1:
            if direction > 0:
                prediction.extend([1])
            else:
                prediction.extend([0])
        else:
            prediction.extend([0])
    #print prediction


    #print [prediction, model.predict(X_test), model.predict_proba(X_test), model.predict_log_proba(X_test)]
    return prediction, model.score(X_test, y_test)

def makeSingleModel(bestdelta, bestlags, fout, cut, end_period, start_test, path_datasets, savemodel, method, parameters, stock):
    """
    returns array of prediction and score from best model.
    """
    lags = range(2, bestlags +1) 
    datasets = loadDatasets(path_datasets, fout)
    delta = range(2, bestdelta +1) 
    datasets = applyRollMeanDelayedReturns(datasets, delta)
    finance = mergeDataframes(datasets, 6, cut, end_period)
    finance = finance.interpolate(method='linear')
    finance = finance.fillna(finance.mean())    
    finance = applyTimeLag(finance, lags, delta)
    X_train, y_train, X_test, y_test  = prepareDataForClassification(finance, start_test)
    print performClassification(X_train, y_train, X_test, y_test, method, parameters, stock, savemodel)


class MarketIntradayPortfolio(Portfolio):
    """Buys or sells 500 shares of an asset at the opening price of
    every bar, depending upon the direction of the forecast, closing 
    out the trade at the close of the bar.

    Requires:
    symbol - A stock symbol which forms the basis of the portfolio.
    bars - A DataFrame of bars for a symbol set.
    signals - A pandas DataFrame of signals (1, -1) for each symbol.
    initial_capital - The amount in cash at the start of the portfolio."""

    def __init__(self, symbol, bars, signals, initial_capital=10000.0, shares=1000):
        self.symbol = symbol        
        self.bars = bars
        self.signals = signals
        self.initial_capital = float(initial_capital)
        self.shares = int(shares)
        self.positions = self.generate_positions()
        
    def generate_positions(self):
        """Generate the positions DataFrame, based on the signals
        provided by the 'signals' DataFrame."""
        positions = pd.DataFrame(index=self.signals.index).fillna(0.0)
        positions[self.symbol] = self.shares*self.signals['signal']
        return positions
                    
    def backtest_portfolio(self):
        """Backtest the portfolio and return a DataFrame containing
        the equity curve and the percentage returns."""
       
        portfolio = pd.DataFrame(index=self.positions.index)
        pos_diff = self.positions.diff()
            
        portfolio['price_diff'] = self.bars['Close_Out']-self.bars['Open_Out']
        portfolio['price_diff'][0:5] = 0.0
        portfolio['profit'] = self.positions[self.symbol] * portfolio['price_diff'] 
     
        portfolio['total'] = self.initial_capital + portfolio['profit'].cumsum()
        portfolio['returns'] = portfolio['total'].pct_change()
        return portfolio


def findBestModel(runs, bestdelta, bestlags, datasets_name, cut, end_period, start_test, path_datasets, savemodel, method, parameters, symbol, period):
    """
    returns array of prediction and score from best model.
    """

    best_return = 0
    bestmodel_name = symbol+'-.pickle'
    stockhist_foname = 'out.csv'
    stockhist_path_name = os.path.join(path_datasets,stockhist_foname)
    getCryptoOut(symbol, start_test, end_period, period).to_csv(stockhist_foname)
    lags = range(1, bestlags) 
    datasets = loadDatasets(path_datasets, datasets_name)
    delta = range(1, bestdelta) 
    datasets = applyRollMeanDelayedReturns(datasets, delta)
    finance = mergeDataframes(datasets, 6, cut, end_period)
    finance = finance.interpolate(method='linear')
    finance = finance.fillna(finance.mean())    
    finance = applyTimeLag(finance, lags, delta)
    X_train, y_train, X_test, y_test  = prepareDataForClassification(finance, start_test)

    # dataframe of stocks historical prices (saved locally from Yahoo Finance)
    bars = pd.read_csv(stockhist_path_name, index_col=0, parse_dates=True)    
    
    # subset of the data corresponding to test set
    bars = bars[start_test:end_period]

    for run in range(1, runs):

        print "Run: ", run, " of ", runs

        print performClassification(X_train, y_train, X_test, y_test, method, parameters, symbol, savemodel)
    
        prediction = getPredictionFromBestModel(bestdelta, bestlags, datasets_name, cut, end_period, start_test, path_datasets, bestmodel_name)
        
        # initialize empty dataframe indexed as the bars. There's going to be perfect match between dates in bars and signals 
        signals = pd.DataFrame(index=bars.index)
        signals = signals.iloc[:-1]
        
        # initialize signals.signal column to zero
        signals['signal'] = 0.0
        
        # copying into signals.signal column results of prediction
        signals['signal'] = prediction[0][:len(signals.index)]
        # shift the prediction forward by one day to match the incoming bars
        signals.signal = signals.signal.shift(1)
        
        # replace the zeros with -1 (new encoding for Down day)
        signals.signal[signals.signal == -1] = 0
        
        # compute the difference between consecutive entries in signals.signal. As
        # signals.signal was an array of 1 and -1 return signals.positions will 
        # be an array of 0s and 2s.
        signals['positions'] = signals['signal'].diff()
        
        # calling portfolio evaluation on signals (predicted returns) and bars 
        # (actual returns)
        portfolio = MarketIntradayPortfolio(symbol, bars, signals)
        
        # backtesting the portfolio and generating returns on top of that 
        returns = portfolio.backtest_portfolio()

        print returns['total'][-1]
        
        if returns['total'][-1] > best_return:
            best_return = returns['total'][-1]
            with open(bestmodel_name, 'rb') as fin:
                bestmodel = cPickle.load(fin)
            fname_out = 'best_'+bestmodel_name
            with open(fname_out, 'wb') as f:
                cPickle.dump(bestmodel, f, -1)

        print best_return

    print best_return
    return

#stock we want to predict
symbol = 'USDT_ETH'
# current working directory is the path to all our files
path_datasets = os.path.dirname(os.path.realpath("__file__"))
datasets_name = 'datasets'+symbol+'.pickle'
#times
cut = datetime.datetime(2015,2,20)
start_test = datetime.datetime(2017,1,1)
end_period = datetime.datetime(2017,5,18)
period = 14400
# maximum lags when testing
maxlags = 9
maxdeltas = 9
bestlags = 20
bestdelta = 20

runs = 40000

#save the model
savemodel = True

'''
RF = random forest, KNN, K nearest neigh, SVM = SMV binary, GTB = Gradient Tree Boosting, RBM = neural network
'''
method = 'RF'
parameters = [4, 8]

findBestModel(runs, bestdelta, bestlags, datasets_name, cut, end_period, start_test, path_datasets, savemodel, method, parameters, symbol, period)
#makeSingleModel(bestdelta, bestlags, datasets_name, cut, start_test, path_datasets, savemodel, method, parameters, symbol)
#performClassifierComparison(maxdeltas, maxlags, datasets_name, cut, start_test, path_datasets, savemodel, method, parameters, symbol)